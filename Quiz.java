
import java.util.*;

public class Quiz {

public static void main(String[] args) {
	int number1 = (int)(Math.random() * 10);
	int number2 = (int)(Math.random() * 10);
	Scanner input = new Scanner(System.in);
	Set<Integer> answers = new HashSet<>();

	System.out.println("What is " + number1 + " + " + number2 + "?");
	int answer = input.nextInt();

	while (number1 + number2 != answer) {
	if (answers.contains(answer)) {
	System.out.println("\nYou already entered number " + answer);
	System.out.println("\nWhat is " + number1 + " + " + number2 + "?");
	} else {
	System.out.println("\nWrong answer. Try again.");
	System.out.println("\nWhat is " + number1 + " + " + number2 + "?");
	answers.add(answer);

	}

	answer = input.nextInt();

	}

	System.out.println("\nYou got the correct answer.");

	input.close();

	}

}
